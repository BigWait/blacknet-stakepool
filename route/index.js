


const Router = require('koa-router');
const router = new Router();
const API = require('../lib/api');

router.get('/', async (ctx, next) => {

    
    await ctx.render('index', { account: config.account});
});



router.get('/recent_blocks', async (ctx, next) => {

    
    await ctx.render('recent_blocks', { recent: true });
});

router.get('/recent_payouts', async (ctx, next) => {

    
    await ctx.render('recent_payouts', { payouts: true });
});


router.get('/account/:account', async (ctx, next) => {

    let account = ctx.params.account;
    let payouts = await Payout.count({account: account});

    let t = await Payout.aggregate(
        [{ $match: {account}},
        { $group: {
            _id: null,
            total: { $sum: "$amount" }
        }}]
    );

    let p = await Payout.aggregate(
        [{ $match: {account, isPayout: true}},
        { $group: {
            _id: null,
            total: { $sum: "$amount" }
        }}]
    );

    let paid = (p[0].total /1e8 ).toFixed(8);
    let total_reward = (t[0].total /1e8 ).toFixed(8);

    
    await ctx.render('account', { account, payouts, total_reward, paid});
});
router.get('/workers', async (ctx, next) => {

    await ctx.render('workers', { workers: true });
});

router.get('/payouts/:blockheight', async (ctx, next) => {

    await ctx.render('payouts', { });
});

module.exports = router;

