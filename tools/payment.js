
// 1. user api get all payment api,
// 2. check height confirm the same height with blnscan
// 3. sync api data & compare with DB data
// 4. make sure the payment is required
// 5. pay it && save it to blockchain

// message remark: PoS Reward Height:#447000


global.config = require('../config');
require('../lib/mongo');
const API = require('../lib/api');
const explorerAPI = require('../lib/explorerapi');
async function processPayment() {

    let page = 1;

    while (true) {
        console.log(`check payments page is ${page}`)

        let txns = await explorerAPI.getTransactions(poolOwner, 0, page);
        if (txns.length === 0) break;

        await checkPayments(txns);

        // let lastone = txns.pop();

        // let payout = await Payout.findOne({ txid: lastone.txid });

        // // if the last one of page list, will break
        // if (payout) break;
        page++;
    }


    let startHeight = config.startHeight;
    let recentBlocks = await explorerAPI.getRecentBlocks();
    let currentHeight = recentBlocks[0].height;
    let queryHeight = currentHeight - config.immature_blocks;

    if (queryHeight < startHeight) return;
    
    while(true){
        let heightQuery = {$gt: startHeight, $lt: queryHeight}, paySuccess = false;
        block = await Transaction.findOne({isPayout: false, type: 254, blockHeight : heightQuery});
       
        if(!block){
            console.log('all payment over');
            break;
        }
        console.log(`Start pay block: #${block.blockHeight}`);

        
        let payouts = await Payout.find({blockHeight: block.blockHeight});

        for(let payout of payouts){

            if(parseInt(payout.amount) == 0) continue;

            if(payout.account == poolOwner) continue;

            if(payout.isPayout === false){
                payout.amount += parseInt(Math.random() * 100);
                let txid = await payIt(payout.account, 'PoS Reward Height:#' + payout.blockHeight , payout.amount);

                if(txid && txid.length == 64){
                    paySuccess = true;
                    payout.txid = txid;
                    payout.isPayout = true;
                    console.log(`${payout.account} pay success, \ntxid is ${txid}`)
                    await payout.save();
                }else{
                    console.log(`payment failed txid is ${txid}`);
                }
            }
        }

        if(paySuccess){
            block.isPayout = true;
            await block.save();
            console.log(`Pay block: #${block.blockHeight} success`);
        }
        await timeout(1000 * 30);
    }

    setTimeout(processPayment,  60 * 1000);
}

processPayment();


async function payIt(account, msg, amount){

    let postdata = {
        mnemonic: mnemonic,
        amount: parseInt(amount),
        fee: 100000,
        to: account,
        message: msg,
        encrypted: 0
    }; 
    let txid = await API.sendMoney(postdata);
    console.log('payit ' + txid);

    return txid;
}


async function checkPayments(txns) {

    for (let tx of txns) {

        if(!tx.data) continue;
        let msg = tx.data.message, height, payout;

        if (msg.length == 0) continue;

        height = getHeightFromMsg(msg);
        if (/^\d+$/.test(height)) {
            // query DB
            let account = tx.to;

            payout = await Payout.findOne({ account, blockHeight: height })

            if(!payout){
                continue;
            }
            if (payout.isPayout == false) {
                payout.isPayout = true;
                payout.txid = tx.txid;
                console.log(`${tx.txid} has been get paid`)
                await payout.save();
            }

            let block = await Transaction.findOne({blockHeight: height, type: 254});
            block.isPayout = true;
            await block.save();
        }
    }
}



function getHeightFromMsg(msg) {

    let base = 'PoS Reward Height:#';

    return msg.replace(base, '');
}

function timeout(delay) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            try {
                resolve(1)
            } catch (e) {
                reject(0)
            }
        }, delay)
    })
}