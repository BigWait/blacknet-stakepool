

const host = "https://www.blnscan.io/api";
const request = require("request");
const rp = require('request-promise');


module.exports = {

    getTransactions:  function(address, type, page){

        return getApi(`/pool/${address}?type=${type}&page=${page}`);
    },

    getRecentBlocks: function(){
        
        return getApi(`/recent_blocks`);
        
    } 
}

function getApi(url){
    return new Promise((resolve) => {
        request({
            url: host + url,
            headers: {
                'User-Agent': 'Mozilla/5.0 (Linux; Android 8.0; Pixel 2 Build/OPD3.170816.012) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Mobile Safari/537.36'
            },
            forever: true,
            json: true
        }, (err, res, body)=>{
            resolve(body);
        });
    });
}



















